package com.anil.roomdbbasicexample_kotlin.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.anil.roomdbbasicexample_kotlin.dao.WordDao
import com.anil.roomdbbasicexample_kotlin.entity.Word
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [Word::class],version = 1)
public abstract class WordRoomDatabase:RoomDatabase() {

    abstract fun wordDao(): WordDao


    private class WordDatabaseCallback(
        private val scope: CoroutineScope
    ):RoomDatabase.Callback(){

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch {
                    var wordDao = database.wordDao()

                    wordDao.deleteAll()
                    var word = Word("Hello")
                    wordDao.insert(word)
                    word = Word("World!")
                    wordDao.insert(word)
                    word = Word("TODO!")
                    wordDao.insert(word)
                }
            }
        }
    }

    companion object{
        @Volatile
        private var INSTANCE: WordRoomDatabase? = null

        fun getDatabase(context: Context,scope: CoroutineScope): WordRoomDatabase? {
            if (INSTANCE==null){
                synchronized(WordRoomDatabase::class){
                    INSTANCE=Room.databaseBuilder(context.applicationContext,WordRoomDatabase::class.java,"word_database")
                        .addCallback(WordDatabaseCallback(scope))
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE=null
        }
    }
}