package com.anil.roomdbbasicexample_kotlin.wordrepository

import androidx.lifecycle.LiveData
import com.anil.roomdbbasicexample_kotlin.dao.WordDao
import com.anil.roomdbbasicexample_kotlin.entity.Word

class WordRepository(private val wordDao: WordDao) {

    val allWords: LiveData<List<Word>> = wordDao.getAllWords()

    suspend fun insert(word: Word) {
        wordDao.insert(word)
    }
}