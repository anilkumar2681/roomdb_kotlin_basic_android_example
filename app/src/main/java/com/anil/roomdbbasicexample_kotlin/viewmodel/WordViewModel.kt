package com.anil.roomdbbasicexample_kotlin.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.anil.roomdbbasicexample_kotlin.database.WordRoomDatabase
import com.anil.roomdbbasicexample_kotlin.entity.Word
import com.anil.roomdbbasicexample_kotlin.wordrepository.WordRepository
import kotlinx.coroutines.launch

class WordViewModel(application: Application):AndroidViewModel(application) {

    private val repository: WordRepository
    val allWords: LiveData<List<Word>>

    init {
        val wordsDao = WordRoomDatabase.getDatabase(application,viewModelScope)!!.wordDao()
        repository = WordRepository(wordsDao)
        allWords = repository.allWords
    }

    fun insert(word: Word) = viewModelScope.launch {
        repository.insert(word)
    }
}